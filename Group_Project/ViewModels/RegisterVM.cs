﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Group_Project.EF;

namespace Group_Project.ViewModels
{
    public class RegisterVm
    {
        EcoImpactEntities db = new EcoImpactEntities();

        public RegisterVm()
        {
            Country = db.Countries.ToList();
            Gender = db.Genders.ToList();
            Question = db.SecurityQuestions.ToList();
        }

        public List<Country> Country { get; set; }
        public List<Gender> Gender { get; set; }
        public List<SecurityQuestion> Question { get; set; }
    }
}