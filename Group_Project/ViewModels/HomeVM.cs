﻿using Group_Project.EF;
using Group_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Group_Project.ViewModels
{
    public class HomeVM
    {
        public UserMaster loggedInUser { get; set; }
        public List<News> newsList { get; set; }
        public List<Tip> tipList { get; set; }
        public List<Activity> activityList { get; set; }
        public List<CategoryMaster> categoryList { get; set; }
        public List<Image> carouselList { get; set; }
        public List<Image> newsImage { get; set; }
        //public List<UserMaster> userList { get; set; }
        public List<Gender> genderList { get; set; }
        public List<Country> countryList { get; set; }
        public List<Badge> userBadgeList { get; set; }
        public List<Image> badgeImageList { get; set; }
        public int points { get; set; }
        public Image leagueBadge { get; set; }
        public Image allUserImages { get; set; }
        public int[] userActivityCount { get; set; }
        public string[] mostPopularActivities { get; set; }
        public List<Image> flags{ get; set; }
        public List<Models.communityMember> communityMembers { get; set; }
    }
}
