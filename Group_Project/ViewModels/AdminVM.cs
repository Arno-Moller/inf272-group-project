﻿using Group_Project.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Group_Project.ViewModels
{
    public class AdminVM
    {
        public List<News> newsList { get; set; }
        public List<Survey> surveyList { get; set; }
        public List<CategoryMaster> categoryList { get; set; }
        public List<Tip> tipList { get; set; }
        public List<Activity> activityList { get; set; }
        public List<Image> carouselList { get; set; }

        public List<Country> countryList { get; set; }
        public List<UserMaster> adminList { get; set; }
        
    }
}