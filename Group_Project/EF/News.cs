//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Group_Project.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class News
    {
        public short NewsID { get; set; }
        public short ImageID { get; set; }
        public string Desctiption { get; set; }
        public System.DateTime Timestamp { get; set; }
        public string Hyperlink { get; set; }
    
        public virtual Image Image { get; set; }
    }
}
