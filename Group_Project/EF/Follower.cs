//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Group_Project.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class Follower
    {
        public short FollowerID { get; set; }
        public short UserID { get; set; }
        public short FriendID { get; set; }
    
        public virtual UserMaster UserMaster { get; set; }
        public virtual UserMaster UserMaster1 { get; set; }
    }
}
