﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;
using Group_Project.EF;
using System.Net;
using Group_Project.ViewModels;

namespace Group_Project.Controllers
{
    public class AdminController : Controller
    {
        public EcoImpactEntities db = new EcoImpactEntities();
        
        private static AdminVM vm = null;

        // GET: Admin
        public ActionResult Index()
        {
            vm = new AdminVM
            {
                newsList = db.News.ToList(),
                surveyList = db.Surveys.ToList(),
                activityList = db.Activities.ToList(),
                carouselList = db.Images.Where(a => a.ImageType.TypeID == 2 ).ToList(),
                countryList = db.Countries.ToList(),
                categoryList = db.CategoryMasters.ToList(),
                adminList = db.UserMasters.Where(a => a.TypeID == 1).ToList()//Get Admin users only
            };

            return View("Admin", vm);
        }

        //public ActionResult Edit(int surveyID, string type)
        //{
        //    if (type == "survey")
        //    {
        //        return PartialView("EditSurvey", new Survey { SurveyID = (short)surveyID, Question = "awe", Points = 10 });
        //    }
        //    if (type == "news")
        //    {
        //        return PartialView("EditNews", new News { NewsID = (short)surveyID });
        //    }
        //    else
        //    {
        //        return View("Index");
        //    }

        //}
        public ActionResult AddNews(string Description, string Hyperlink, HttpPostedFileBase postedNewsFile)
        {
            string imgPath = "~/Images/" + postedNewsFile.FileName;
            postedNewsFile.SaveAs(Path.Combine(HttpContext.Server.MapPath("~/Images/"), postedNewsFile.FileName));
            
            EF.Image image = new EF.Image();
            image.Timestamp = System.DateTime.Now;
            image.Path = imgPath;
            image.TypeID = 1;
            db.Images.Add(image);

            News n = new News();
            n.ImageID = image.ImageID;
            n.Timestamp = System.DateTime.Now;
            n.Hyperlink = Hyperlink;
            n.Desctiption = Description;

            db.News.Add(n);
            db.SaveChanges();
            
            return RedirectToAction("Index");
        }
        
        public ActionResult EditNews( int newsID, string Description, string Hyperlink, HttpPostedFileBase postedNewsFile)
        {
            
            try
            {
                if (ModelState.IsValid)
                {
                    News n = db.News.Where(z => z.NewsID == newsID).FirstOrDefault();
                    
                    string imgPath = "~/Images/" + postedNewsFile.FileName;
                    postedNewsFile.SaveAs(Path.Combine(HttpContext.Server.MapPath("~/Images/"), postedNewsFile.FileName));

                    EF.Image image = db.Images.Where(z => z.ImageID == n.ImageID).FirstOrDefault();
                    n.ImageID = image.ImageID;
                    n.Timestamp = System.DateTime.Now;
                    n.Hyperlink = Hyperlink;
                    n.Desctiption = Description;

                    image.Timestamp = System.DateTime.Now;
                    image.Path = imgPath;
                    image.TypeID = 1;
                    db.Images.Add(image);
                    db.Entry(image).State = System.Data.Entity.EntityState.Modified;

                    db.News.Add(n);
                    db.Entry(n).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges(); 
                }
                return RedirectToAction("Index");
            }
            catch
            {
                //return ViewBag.Message();
                return RedirectToAction("Index");
            }
        }

        public JsonResult DeleteNews(int id)
        {
            News deleteNews = db.News.Where(z=>z.NewsID == id).FirstOrDefault();
            //EF.Image deleteImage = db.Images.Find(deleteNews.ImageID);
            
            //db.Images.Remove(deleteImage);
            db.News.Remove(deleteNews);
            db.SaveChanges();
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddSurvey(string Question, int Points)//add survey item
        {
            Survey s = new Survey();
            s.Question = Question;
            s.Points = (short)Points;
            db.Surveys.Add(s);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        
        public ActionResult SaveEditSurvey( int surveyID, string Question, int Points)
        {
            try
            {
                Survey s = db.Surveys.Find(surveyID);
                if (ModelState.IsValid)
                {
                    
                    s.Question = Question;
                    s.Points = (short)Points;
                    db.Surveys.Add(s);
                    db.Entry(s).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                e = ViewBag.Message;
                return RedirectToAction("Index");
            }
        }
        
        public ActionResult AddCategory (string Description)
        {
            CategoryMaster c = new CategoryMaster();
            c.Description = Description;
            db.CategoryMasters.Add(c);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AddTip(int catID, string Description)
        {

            Tip t = new Tip();
            t.Description = Description;
            t.CategoryID = (short)catID; 
            db.Tips.Add(t);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AddActivity(int catID, string Description)
        {
            Activity t = new Activity();
            t.CategoryID = (short)catID;
            t.Description = Description;
            db.Activities.Add(t);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult EditTip(int TipID, string Description)
        {
            try
            {
                Tip tipToEdit = db.Tips.Find(TipID);
                if (ModelState.IsValid)
                {
                    tipToEdit.Timestamp = System.DateTime.Now;
                    tipToEdit.Description = Description;
                    db.Tips.Add(tipToEdit);
                    db.Entry(tipToEdit).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult EditActivity(int actID, string Description)
        {
            try
            {
                Activity actToEdit = db.Activities.Find(actID);
                if (ModelState.IsValid)
                {
                    actToEdit.Description = Description;
                    db.Activities.Add(actToEdit);
                    db.Entry(actToEdit).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult EditCategory(int catID, string Description)
        {
            try
            {
                
                CategoryMaster c = db.CategoryMasters.Where(z => z.CategoryID == catID).FirstOrDefault();
                if (ModelState.IsValid)
                {
                    c.Description = Description;
                    db.CategoryMasters.Add(c);
                    db.Entry(c).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                e = ViewBag.Message;
                return RedirectToAction("Index");
            }
        }
        public JsonResult DeleteSurvey(int id)
        {
            Survey deleteSurvey = db.Surveys.Where(z=>z.SurveyID == id).FirstOrDefault();
            db.Surveys.Remove(deleteSurvey);
            db.SaveChanges();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteCarousel(int id)
        {
            EF.Image deleteCarousel = db.Images.Where(z => z.ImageID == id).FirstOrDefault();
            db.Images.Remove(deleteCarousel);
            db.SaveChanges();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteCategory(int id)
        {
            CategoryMaster deleteCategory = db.CategoryMasters.Find(id);
            db.CategoryMasters.Remove(deleteCategory);
            db.SaveChanges();
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteTip(int id)
        {
            Tip deleteTip = db.Tips.Find(id);
            db.Tips.Remove(deleteTip);
            db.SaveChanges();
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteActivity(int id)
        {
            Activity deleteAct = db.Activities.Find(id);
            db.Activities.Remove(deleteAct);
            db.SaveChanges();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteAdmin(int id)
        {
            UserMaster deleteAdmin = db.UserMasters.Find(id);
            db.UserMasters.Remove(deleteAdmin);
            db.SaveChanges();
            return Json("", JsonRequestBehavior.AllowGet);
        }


        //Add Admin
        public ActionResult AddAdmin(string Username, string Email)
        {
            db.Configuration.ProxyCreationEnabled = false;
            UserMaster a = new UserMaster();
            a.TypeID = 1;
            a.CountryID = 188;
            a.ImageID = 11;
            a.GenderID = 1;
            a.SecurityID = 2;
            a.LeagueLevel = 1;
            a.Username = Username;
            a.Email = Email;
            a.Password = "admin";
            a.SecurityAnswer = "Mrs Cool";
            a.Multiplier = 0;
            a.DailyGoal = 0;
            a.Streak = 0;
            a.Privacy = false;
            db.UserMasters.Add(a);
            db.SaveChanges();
            //((System.Data.Entity.Validation.DbEntityValidationException)$exception).EntityValidationErrors

            return RedirectToAction("Index");

        }

        public ActionResult AddCarousel(HttpPostedFileBase postedNewsFile)
        {
            string imgPath = "~/Images/" + postedNewsFile.FileName;
            postedNewsFile.SaveAs(Path.Combine(HttpContext.Server.MapPath("~/Images/"), postedNewsFile.FileName));

            EF.Image image = new EF.Image();
            image.Timestamp = System.DateTime.Now;
            image.Path = imgPath;
            image.TypeID = 2;
            db.Images.Add(image);
            
            db.SaveChanges();

            return RedirectToAction("Index");

        }
        public ActionResult EditCarousel(int carID, HttpPostedFileBase postedNewsFile)
        {
            string imgPath = "~/Images/" + postedNewsFile.FileName;
            postedNewsFile.SaveAs(Path.Combine(HttpContext.Server.MapPath("~/Images/"), postedNewsFile.FileName));

            EF.Image image = db.Images.Where(z => z.ImageID == carID).FirstOrDefault();
            image.Timestamp = System.DateTime.Now;
            image.Path = imgPath;
            image.TypeID = 2;
            db.Images.Add(image);
            db.Entry(image).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");

        }

        



    }
}