﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Group_Project.EF;
using Group_Project.Models;

namespace Group_Project.Controllers
{
    public class LoginController : Controller
    {

        EcoImpactEntities db = new EcoImpactEntities();
        static UserMaster user;     //use in forgot password
        static int otp;             //use in forgot password

        // GET: View
        public ActionResult Index()
        {
            return PartialView();// View();
        }

        //login to account - set session variable
        public ActionResult Login(string username, string password)
        {
            //hash the users password
            MD5 md5 = new MD5CryptoServiceProvider();
            Byte[] originalBytes = ASCIIEncoding.Default.GetBytes(password);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);

            string pw = Convert.ToBase64String(encodedBytes);
            if(pw.Length > 64)
                pw = pw.Substring(0, 63);

            UserMaster u = db.UserMasters.FirstOrDefault(us => us.Username.Replace(" ", "").ToLower() == username.ToLower() || us.Email.Replace(" ", "").ToLower() == username.ToLower());

            if(u == null)
            {
                //invalid username
                Session["UserID"] = null;
                ViewBag.invalid = "username";
            }
            else
            {
                if(u.Password.Replace(" ", "") != pw.Replace(" ", ""))
                {
                    //invalid password
                    Session["UserID"] = null;
                    ViewBag.invalid = "password";
                }
                else
                {

                    Session["UserID"] = u.UserID;

                    //admin user
                    if (u.TypeID == 1)
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    //normal user
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }

            return View("Index");
        }

        //sends email to user with otp
        public ActionResult ResetPassword(int question, string answer, string email)
        {
            user = db.UserMasters.Where(u => u.Email.Replace(" ", "") == email).FirstOrDefault();

            if(user == null)
            {
                ViewBag.invalid = "invalid email";

                List<SecurityQuestion> q = db.SecurityQuestions.ToList();

                return View("ForgotPassword", q);
            }
            else
            {
                if(user.SecurityID != question)
                {
                    ViewBag.invalid = "invalid question";

                    List<SecurityQuestion> q = db.SecurityQuestions.ToList();

                    return View("ForgotPassword", q);
                }
                else
                {
                    if (user.SecurityAnswer.Replace(" ", "").ToLower() != answer.ToLower())
                    {
                        ViewBag.invalid = "invalid answer";

                        List<SecurityQuestion> q = db.SecurityQuestions.ToList();

                        return View("ForgotPassword", q);
                    }
                }
            } 

            try
            {
                Random rnd = new Random();
                otp = rnd.Next(1000000, 9999999);

                GMailer.GmailUsername = "ecoImpactWeb@gmail.com";
                GMailer.GmailPassword = "ecoImpact2020";

                GMailer mailer = new GMailer();
                mailer.ToEmail = user.Email.Replace(" ", "");
                mailer.Subject = "EcoImpact Reset Password OTP";
                mailer.Body = "<p>Please go to the EcoImpact website and enter the following One Time Pin:<p>  <a>"+ otp +"</a>";
                mailer.IsHtml = true;
                mailer.Send();

            }
            catch (Exception ex)
            {

                throw;
            }
           

            return View("ResetPassword");
        }

        //updates the password
        public ActionResult UpdatePassword(int userOTP, string userPassword)
        {
            if(userOTP != otp)
            {
                ViewBag.invalid = "invalid";
                return View("ResetPassword");
            }

            //hash the users password
            MD5 md5 = new MD5CryptoServiceProvider();
            Byte[] originalBytes = ASCIIEncoding.Default.GetBytes(userPassword);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);

            string pw = Convert.ToBase64String(encodedBytes);
            if (pw.Length > 64)
                pw = pw.Substring(0, 64);

            ViewBag.invalid = "valid";

            var u = db.UserMasters.FirstOrDefault(us => us.Email == user.Email);

            u.Password = pw;

            db.SaveChanges();

            return View("ResetPassword");
        }
        
        //return forgot password view
        public ActionResult ForgotPassword()
        {
            List<SecurityQuestion> question = db.SecurityQuestions.ToList();
            return View("ForgotPassword", question);
        }
    }
}