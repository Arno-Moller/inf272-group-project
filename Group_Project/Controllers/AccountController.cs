﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Group_Project.EF;
using Group_Project.ViewModels;
using Group_Project.Models;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Group_Project.Controllers
{
    
    public class AccountController : Controller
    {
        EcoImpactEntities db = new EcoImpactEntities();
        static RegisterUser user;
        static Image profilePic;

        //use for register user
        public ActionResult Index()
        {
            //default image path if user does not upload image on account creation
            string path = "Players/defaultProfile.png";

            if(profilePic != null)
            {
                path = profilePic.Path;
            }
            ViewBag.path = path;

            RegisterVm model = new RegisterVm();

            return View("CreateAccount", model);
        }

        //save user details if they choose to take the survey
        [HttpPost]
        public ActionResult Survey(string[] u)
        {
            if (user == null)
            {
                user = new RegisterUser(u[0], u[1], u[2], u[3], Int32.Parse(u[4]), u[5], Int32.Parse(u[6]), Int32.Parse(u[7]), Int32.Parse(u[8]));
            }
            else
            {
                user.Name = u[0];
                user.Email = u[1];
                user.Pw = u[2];
                user.PrivateOrPublic = u[3];
                user.Selected_question = Int32.Parse(u[4]);
                user.Answer = u[5];
                user.Country = Int32.Parse(u[6]);
                user.Gender = Int32.Parse(u[7]);

            }

            //check if user exists
            UserMaster userToTest = db.UserMasters.FirstOrDefault(us => us.Username.Replace(" ", "").ToLower().Equals(user.Name.ToLower()) || us.Email.Replace(" ", "").ToLower().Equals(user.Name.ToLower()));

            if (userToTest != null)
                return Json(false);

            return Json(true);
        }

        //register new user
        [HttpPost]
        public ActionResult SubmitRegister(string[] u)
        {
            if (user == null)
            {
                user = new RegisterUser(u[0], u[1], u[2], u[3], Int32.Parse(u[4]), u[5], Int32.Parse(u[6]), Int32.Parse(u[7]), Int32.Parse(u[8]));
            }
            else
            {
                user.Name = u[0];
                user.Email = u[1];
                user.Pw = u[2];
                user.PrivateOrPublic = u[3];
                user.Selected_question = Int32.Parse(u[4]);
                user.Answer = u[5];
                user.Country = Int32.Parse(u[6]);
                user.Gender = Int32.Parse(u[7]);
            }



            //check if user exists
            UserMaster userToTest = db.UserMasters.FirstOrDefault(us => us.Username.Replace(" ", "").ToLower().Equals(user.Name.ToLower()) || us.Email.Replace(" ", "").ToLower().Equals(user.Name.ToLower()));

            if (userToTest != null)
                return Json(false);

            bool added = addUser();

            return Json(added);
        }

        //adds user to database
        public bool addUser()
        {

            //hash the users password
            MD5 md5 = new MD5CryptoServiceProvider();
            Byte[] originalBytes = ASCIIEncoding.Default.GetBytes(user.Pw);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);

            string password = Convert.ToBase64String(encodedBytes);

            if(password.Length > 64)
                password = password.Substring(0,63);

            bool userPrivacy = false;
            if (user.PrivateOrPublic == "private")
            {
                userPrivacy = true;
            }

            short imageID = 26;
            if (profilePic != null)
                imageID = profilePic.ImageID;

            UserMaster u = new UserMaster
            {
                CountryID = Convert.ToInt16(user.Country),
                GenderID = Convert.ToInt16(user.Gender),
                SecurityID = Convert.ToInt16(user.Selected_question),
                ImageID = imageID,
                LeagueLevel = 1,
                TypeID = 2,
                Username = user.Name,
                Email = user.Email,
                Password = password,
                SecurityAnswer = user.Answer,
                Multiplier = Convert.ToInt16(user.Multiplier),
                DailyGoal = Convert.ToInt16(user.Goal),
                Streak = 0,
                Privacy = userPrivacy
            };

            db.UserMasters.Add(u);

            DateTime time = DateTime.Now;

            db.UserLeagues.Add(new UserLeague {
                UserID = u.UserID,
                LeagueID = 1,
                Timestamp = time,
                Position = (short) (db.UserLeagues.Where(ul => ul.LeagueID == 1).Count() + 1)
            });

            try
            {
                db.SaveChanges();
                Session["UserID"] = u.UserID;
            }
            catch (Exception)
            {
                return false;
            }
            

            user = null;
            profilePic = null;

            return true;
        }

        //submit user survey
        public ActionResult SubmitSurvey(int points)
        {

            user.Multiplier = points;

            addUser();

            return RedirectToAction("Index", "Home");
        }

        //register user and redirect to home page
        public ActionResult SkipSurvey()
        {
            if(user != null)
            {
                user.Multiplier = 0;

                addUser();
            }            

            return RedirectToAction("Index", "Home");
        }

        //display user survey page
        public ActionResult returnSurvey()
        {
            ViewBag.update = false;

            List<Survey> survey = db.Surveys.ToList();

            return View("Survey", survey);
        }
        
        //display user survey page to update
        public ActionResult UpdateUserSurvey()
        {
            ViewBag.update = true;

            List<Survey> survey = db.Surveys.ToList();

            return View("Survey", survey);
        } 

        public ActionResult Update(int points)
        {
            if (Session["UserID"] != null)
            {
                object idObject = Session["UserID"];

                int id = Convert.ToInt32(idObject);

                UserMaster u = db.UserMasters.FirstOrDefault(i => i.UserID == id);

                u.Multiplier = (short) points;

                db.SaveChanges();

            }

            return RedirectToAction("Index", "Home");
        }

        //------------ Edit User ------------------//


        //diplay edit view
        public ActionResult Edit()
        {
            user = null;
            profilePic = null;

            if (Session["UserID"] != null)
            {
                object idObject = Session["UserID"];

                int id = Convert.ToInt32(idObject);

                UserMaster u = db.UserMasters.FirstOrDefault(us => us.UserID == (int)id);

                //if the user is not found, log the user out
                if (u == null)
                {
                    Session["UserID"] = null;

                    return RedirectToAction("Index", "Login");
                }
                else
                {
                    RegisterUser regUser = new RegisterUser(u.Username.Replace(" ", ""), u.Email, "", u.Privacy == true ? "private" : "public", u.SecurityID, u.SecurityAnswer, u.CountryID, u.GenderID, (int)u.DailyGoal);
                    RegisterVm model = new RegisterVm();

                    Image img = db.Images.FirstOrDefault(i => i.ImageID == u.ImageID);
                    ViewBag.Image = img.Path;
                    ViewBag.user = regUser;

                    return View("EditAccount", model);
                }
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
            

            //return View("EditAccount");
        }

        //upload image
        [HttpPost]
        public ActionResult UploadImage(HttpPostedFileBase image)
        {
            try
            {
                string fileName = Path.GetFileNameWithoutExtension(image.FileName);
                string extention = Path.GetExtension(image.FileName);

                DateTime time = DateTime.Now;
                string date = time.ToString();
                date = date.Replace(" ", "");
                date = date.Replace("/", "");
                date = date.Replace(":", "");

                if(fileName.Length > 60)
                {
                    fileName = fileName.Substring(0, 59);
                }

                fileName = fileName + date + extention;

                string path = "\\Images\\Players\\" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Images/Players/"), fileName);
                image.SaveAs(fileName);

                profilePic = new Image { TypeID = 5, Timestamp = time, Path = path };

                db.Images.Add(profilePic);
                db.SaveChanges();

                

            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                throw;
            }

            return Json(true);
        }


        public ActionResult UpdateUser(string[] u)
        {
            string name = u[0];

            if (Session["UserID"] != null)
            {
                object idObject = Session["UserID"];

                int id = Convert.ToInt32(idObject);

                //check if user exists
                UserMaster userToTest = db.UserMasters.FirstOrDefault(us => us.Username.Replace(" ", "").ToLower().Equals(name.ToLower()) && us.UserID != id);

                if (userToTest != null)
                    return Json(false);
                else
                {
                    UserMaster updateUser = db.UserMasters.FirstOrDefault(us => us.UserID == id);

                    //hash the users password
                    MD5 md5 = new MD5CryptoServiceProvider();
                    Byte[] originalBytes = ASCIIEncoding.Default.GetBytes(u[1]);
                    Byte[] encodedBytes = md5.ComputeHash(originalBytes);

                    string password = Convert.ToBase64String(encodedBytes);

                    if (password.Length > 64)
                        password = password.Substring(0, 63);

                    if(u[0] != "")
                        updateUser.Username = u[0];
                    if (u[1] != "")
                        updateUser.Password = password;
                    updateUser.Privacy = u[2] == "private" ? true : false;
                    updateUser.SecurityID = (short) Int32.Parse(u[3]);
                    if (u[4].Replace(" ", "") != "")
                        updateUser.SecurityAnswer = u[4];
                    updateUser.CountryID = (short) Int32.Parse(u[5]);
                    updateUser.GenderID = (short) Int32.Parse(u[6]);
                    updateUser.DailyGoal = (short) Int32.Parse(u[7]);

                    if (profilePic != null)
                        updateUser.ImageID = profilePic.ImageID;

                    db.SaveChanges();
                    
                }
                   
            }


            return Json(true);
        }




        //don't think they ever get used
        private SelectList CountrySelectList()
        {
            SelectList s = new SelectList(db.Countries.ToList(), "CountryID", "Name");
            return s;
        }
        private SelectList GenderSelectList()
        {
            SelectList s = new SelectList(db.Genders.ToList(), "GenderID", "Description");
            return s;
        }
        private SelectList QuestionsSelectList()
        {
            SelectList s = new SelectList(db.SecurityQuestions.ToList(), "SecurityID", "Question");
            return s;
        }
    }
}