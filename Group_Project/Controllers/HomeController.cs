﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Group_Project.EF;
using Group_Project.Models;
using Group_Project.ViewModels;

namespace Group_Project.Controllers
{
    public class HomeController : Controller
    {
        public EcoImpactEntities db = new EcoImpactEntities();

        private static HomeVM vm = null;
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                //get the logged in user's id
                object idObject = Session["UserID"];

                int id = Convert.ToInt32(idObject);

                //get the logged in user
                UserMaster user = db.UserMasters.FirstOrDefault(u => u.UserID == id);

                //get user badges
                List<UserBadge> badgeList = db.UserBadges.Where(ub => ub.UserID == id).ToList();
                List<Badge> badges = new List<Badge>();
                foreach (var userBadge in badgeList)
                {
                    badges.Add(db.Badges.FirstOrDefault(b => b.BadgeID == userBadge.BadgeID));
                }

                //get all the user's points for the day
                int points = getUserWeeklyPoints(id);

                //get the user's leauge
                League league = db.Leagues.FirstOrDefault(l => l.LeagueID == db.UserLeagues.FirstOrDefault(ulist => ulist.UserID == id).LeagueID);
                List<Image> leageBadges = db.Images.Where(i => i.TypeID == 4).ToList();


                //populate the viewmodel
                vm = new HomeVM
                {
                    leagueBadge = leageBadges[league.LeagueID-1],
                    points = points,
                    loggedInUser = user,
                    newsList = db.News.ToList(),
                    tipList = db.Tips.ToList(),
                    activityList = db.Activities.ToList(),
                    categoryList = db.CategoryMasters.ToList(),
                    carouselList = db.Images.Where(i => i.TypeID == 2).ToList(),    //get all carousel images
                    newsImage = db.Images.Where(i => i.TypeID == 1).ToList(),
                    //userList = db.UserMasters.Where(a => a.TypeID == 2).ToList(),    //Get Player users only
                    genderList = db.Genders.ToList(),
                    countryList = db.Countries.ToList(),
                    userBadgeList = badges,
                    badgeImageList = db.Images.Where(i => i.TypeID == 3).ToList(),
                    userActivityCount = getUserActivityCount(id),
                    mostPopularActivities = getPopularActivities(),
                    flags = db.Images.Where(e => e.TypeID == 6).ToList(),
                    communityMembers = getCommunity(id)
                };

                //set the use's profile image
                Image img = db.Images.FirstOrDefault(i => i.ImageID == user.ImageID);

                ViewBag.profileImage = img.Path;

                //return the view
                return View("Home", vm);

            }
           
             return RedirectToAction("Index", "Login");
            

        }

        //handle activity addition and removal, adding and removing badges
        public ActionResult AddSubtractDailyPoints(string[] arr)
        {
            if (Session["UserID"] != null)
            {
                object idObject = Session["UserID"];

                int id = Convert.ToInt32(idObject);

                UserMaster userToBeUpdated = db.UserMasters.Where(u => u.UserID == (short)id).FirstOrDefault();

                var yesterday = GetFirstDayOfWeek(DateTime.Today);

                int prevPoints = 0;
                List<UserActivity> pastDayActivities = db.UserActivities.Where(a => a.UserID == id && a.Timestamp > yesterday).ToList();
                foreach (var activity in pastDayActivities)
                {
                    Activity act = db.Activities.FirstOrDefault(f => f.ActivityID == activity.ActivityID);
                    prevPoints += act.Points;
                }

                DateTime time = DateTime.Now;
                int activityID = Int32.Parse(arr[1]);


                if (arr[0].Equals("add"))
                {
                    db.UserActivities.Add(new UserActivity
                    {
                        UserID = (short)id,
                        ActivityID = (short)activityID,
                        Timestamp = time
                    });

                    db.SaveChanges();
                }
                else
                {


                    var userActivity = db.UserActivities.FirstOrDefault(a => a.Timestamp > yesterday && a.UserID == (short)id && a.ActivityID == (short)activityID);

                    db.UserActivities.Remove(userActivity);
                    db.SaveChanges();

                }




                //streak calculation
                int postPoints = 0;
                List<UserActivity> DayActivities = db.UserActivities.Where(a => a.UserID == id && a.Timestamp > yesterday).ToList();
                foreach (var activity in DayActivities)
                {
                    Activity act = db.Activities.FirstOrDefault(f => f.ActivityID == activity.ActivityID);
                    postPoints += act.Points;
                }

                if (postPoints >= userToBeUpdated.DailyGoal)
                {
                    //if the users daily goal is reached
                    if (prevPoints < userToBeUpdated.DailyGoal)
                    {
                        userToBeUpdated.Streak++;
                        db.SaveChanges();

                        //add badge if user achived one
                        if (addBadge((int)userToBeUpdated.Streak, id))
                        {
                            return Json("badge");
                        }



                        return Json("reached");
                    }
                }
                else if (postPoints < userToBeUpdated.DailyGoal)
                {
                    //if the users daily goal was reached, but no more
                    if (prevPoints >= userToBeUpdated.DailyGoal)
                    {
                        userToBeUpdated.Streak--;
                        db.SaveChanges();

                        removeBadge(prevPoints, postPoints, id);

                        return Json("short");
                    }
                }


            }
            return Json(true);
        }

        //get nr of user activities per category
        private int[] getUserActivityCount(int id)
        {
            int[] nr = { 0, 0, 0, 0, 0 };
            List<UserActivity> uActivities = db.UserActivities.ToList();    //.Where(u => u.UserID == id)

            foreach (var activity in uActivities)
            {
                nr[db.Activities.First(a => a.ActivityID == activity.ActivityID).CategoryID - 1]++;
            }

            return nr;
        }

        //get most popular activities per category
        private string[] getPopularActivities()
        {
            string[] popular = { "", "", "", "", "" };
            List<UserActivity> userActivity = db.UserActivities.ToList();
            List<Activity> activity = db.Activities.ToList();

            int i = 0;
            int highest = 0;
            int id = 0;

            for (int k = 0; k < 5; k++)
            {
                i = 0;
                highest = -1;
                id = 0;

                foreach (var a in activity.Where(l => l.CategoryID == k + 1))
                {
                    i = userActivity.Where(c => c.ActivityID == a.ActivityID).Count();
                    if (i > highest)
                    {
                        highest = i;
                        id = a.ActivityID;
                    }
                }
                popular[k] = activity.Find(f => f.ActivityID == id).Description;
            }


            return popular;
        }

        //add appropriate badge
        private bool addBadge(int streak, int id)
        {
            try
            {
                DateTime time = DateTime.Now;

                //add first badge
                if (streak == 1)
                {
                    db.UserBadges.Add(new UserBadge
                    {
                        UserID = (short)id,
                        BadgeID = 1,
                        Timestamp = time
                    });
                    db.SaveChanges();

                    return true;
                }
                else if (streak == 2)
                {
                    db.UserBadges.Add(new UserBadge
                    {
                        UserID = (short)id,
                        BadgeID = 2,
                        Timestamp = time
                    });
                    db.SaveChanges();

                    return true;
                }
                else if (streak == 3)
                {
                    db.UserBadges.Add(new UserBadge
                    {
                        UserID = (short)id,
                        BadgeID = 3,
                        Timestamp = time
                    });
                    db.SaveChanges();

                    return true;
                }
                else if (streak == 4)
                {
                    db.UserBadges.Add(new UserBadge
                    {
                        UserID = (short)id,
                        BadgeID = 4,
                        Timestamp = time
                    });
                    db.SaveChanges();

                    return true;
                }
                else if (streak == 8)
                {
                    db.UserBadges.Add(new UserBadge
                    {
                        UserID = (short)id,
                        BadgeID = 5,
                        Timestamp = time
                    });
                    db.SaveChanges();

                    return true;
                }
                else if (streak == 24)
                {
                    db.UserBadges.Add(new UserBadge
                    {
                        UserID = (short)id,
                        BadgeID = 6,
                        Timestamp = time
                    });
                    db.SaveChanges();

                    return true;
                }


            }
            catch (Exception)
            {

            }

            return false;
        }

        //remove appropriate badge
        private void removeBadge(int prev, int curr, int id)
        {
            try
            {
                DateTime time = DateTime.Now;

                //add first badge
                if (prev == 1)
                {
                    UserBadge b = new UserBadge { UserID = (short)id, BadgeID = 1 };
                    db.UserBadges.Remove(b);

                }
                else if (prev == 2)
                {
                    UserBadge b = new UserBadge { UserID = (short)id, BadgeID = 2 };
                    db.UserBadges.Remove(b);

                }
                else if (prev == 3)
                {
                    UserBadge b = new UserBadge { UserID = (short)id, BadgeID = 3 };
                    db.UserBadges.Remove(b);

                }
                else if (prev == 4)
                {
                    UserBadge b = new UserBadge { UserID = (short)id, BadgeID = 4 };
                    db.UserBadges.Remove(b);

                }
                else if (prev == 8)
                {
                    UserBadge b = new UserBadge { UserID = (short)id, BadgeID = 5 };
                    db.UserBadges.Remove(b);

                }
                else if (prev == 24)
                {
                    UserBadge b = new UserBadge { UserID = (short)id, BadgeID = 6 };
                    db.UserBadges.Remove(b);

                }

                db.SaveChanges();


            }
            catch (Exception)
            {

            }

        }

        private int getUserWeeklyPoints(int id)
        {
            //the start of the day - 00:00
            //var yesterday = DateTime.Today;
            DateTime time = DateTime.Now;
            DateTime startOfWeek = GetFirstDayOfWeek(time);

            int points = 0;
            List<UserActivity> pastDayActivities = db.UserActivities.Where(a => a.UserID == id && a.Timestamp > startOfWeek).ToList();
            foreach (var activity in pastDayActivities)
            {
                Activity act = db.Activities.FirstOrDefault(f => f.ActivityID == activity.ActivityID);
                points += act.Points;
            }
            return points;
        }

        private int getUserDailyPoints(int id)
        {
            DateTime time = DateTime.Today;

            int points = 0;
            List<UserActivity> pastDayActivities = db.UserActivities.Where(a => a.UserID == id && a.Timestamp > time).ToList();
            foreach (var activity in pastDayActivities)
            {
                Activity act = db.Activities.FirstOrDefault(f => f.ActivityID == activity.ActivityID);
                points += act.Points;
            }
            return points;
        }

        public List<communityMember> getCommunity(int id)
        {
            List<Image> leageBadges = db.Images.Where(i => i.TypeID == 4).ToList();

            List<communityMember> members = new List<communityMember>();

            List<UserMaster> users = db.UserMasters.Where(u => u.UserID != id && u.Privacy == false).ToList();

            foreach(var u in users)
            {
                members.Add(new communityMember
                {
                    ProfilePic = db.Images.First(i => i.ImageID == u.ImageID).Path,
                    Name = u.Username,
                    CountryFlag = db.Images.FirstOrDefault(i => i.ImageID == db.Countries.FirstOrDefault(c => c.CountryID == u.CountryID).ImageID).Path,
                    Points = getUserWeeklyPoints(u.UserID),
                    LeagueIcon = leageBadges[u.LeagueLevel - 1].Path,
                    ID = u.UserID,
                    Following = db.Followers.Where(f => f.UserID == id && f.FriendID == u.UserID).Any()
            });
            }

            return members;
        }

        [HttpPost]
        public ActionResult Follow(string[] data)
        {
            if (Session["UserID"] != null)
            {
                //get the logged in user's id
                object idObject = Session["UserID"];

                int id = Convert.ToInt32(idObject);

                if (data[1].Equals("True"))
                {
                    db.Followers.Add(new Follower
                    {
                        UserID = (short)id,
                        FriendID = (short)Int32.Parse(data[0])
                    });
                }
                else
                {
                    id = (short)id;
                    short friendID = (short)Int32.Parse(data[0]);


                    var follower = db.Followers.FirstOrDefault(f => f.UserID == id && f.FriendID == friendID);

                    db.Followers.Remove(follower);
                }

                db.SaveChanges();
            }
            return Json(true);
        }

        //get first day of specified week
        public static DateTime GetFirstDayOfWeek(DateTime dayInWeek)
        {
            CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            DayOfWeek firstDay = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            DateTime firstDayInWeek = dayInWeek.Date;
            while (firstDayInWeek.DayOfWeek != firstDay)
                firstDayInWeek = firstDayInWeek.AddDays(-1);

            return firstDayInWeek;
        }

        [HttpPost]
        public JsonResult GetLeagueStats()
        {
            if (Session["UserID"] != null)
            {
                //get the logged in user's id
                object idObject = Session["UserID"];

                int id = Convert.ToInt32(idObject);

                List<Image> leageBadges = db.Images.Where(i => i.TypeID == 4).ToList();

                List<LeagueUsers> members = new List<LeagueUsers>();

                List<UserMaster> users = db.UserMasters.Where(u => u.LeagueLevel == db.UserMasters.FirstOrDefault(i => i.UserID == id).LeagueLevel).ToList();



                foreach (var u in users)
                {
                    members.Add(new LeagueUsers
                    {
                        ProfilePic = db.Images.First(i => i.ImageID == u.ImageID).Path,
                        ID = u.UserID,
                        Name = u.Username,
                        Points = getUserWeeklyPoints(u.UserID),
                        LeagueIcon = leageBadges[u.LeagueLevel - 1].Path,
                    });
                }

                members.Sort((p, c) => c.Points.CompareTo(p.Points));

                int pos = 1;
                foreach (var m in members)
                {
                    m.Position = pos++;
                }

                PerformanceModel l = new PerformanceModel();
                l.highest = members[0].Points;
                l.myIndex = members.Find(k => k.ID == id).Position;
                l.DailyPoints = getUserDailyPoints(id);
                l.LeagueName = db.Leagues.FirstOrDefault(d => d.LeagueID == db.UserMasters.FirstOrDefault(p => p.UserID == id).LeagueLevel).LeagueName;

                members.Sort((c, p) => c.Name.CompareTo(p.Name));

                l.leagueUsers = members;





                List<UserActivity> userA = db.UserActivities.Where(u => u.UserID == id).OrderBy(i => i.Timestamp).ToList();

                int[] done = new int[userA.Count()];
                for (int i = 0; i < done.Length; i++)
                    done[i] = 0;

                int weekly = 0;
                int[] categories = { 0,0,0,0,0};
                List<WeeklyActivity> weeklyactivities = new List<WeeklyActivity>();

                foreach(var val in userA)
                {
                    if(done[getIndex(userA, val)] == 0)
                    {
                        DateTime t = GetFirstDayOfWeek(val.Timestamp);
                        List<UserActivity> q = userA.Where(c => c.Timestamp >= t && c.Timestamp < t.AddDays(7)).ToList();

                        weekly = 0;
                        //Array.Clear(categories, 0, categories.Length);
                        foreach (var v in q)
                        {
                            done[getIndex(userA, v)] = 1;

                            categories[db.Activities.FirstOrDefault(w => w.ActivityID == v.ActivityID).CategoryID - 1]++;

                            weekly += db.Activities.FirstOrDefault(a => a.ActivityID == v.ActivityID).Points;
                        }
                        weeklyactivities.Add(new WeeklyActivity { Total = weekly, Week = t, Date = t.ToString().Substring(0, t.ToString().IndexOf(" ")) });
                    }
                        
                }

                int highestDaily = 0, lowestDaily = 0, totalDaily = 0, temp = 0, countDaily = 0;
                foreach (var val in userA)
                {
                    totalDaily += db.Activities.FirstOrDefault(a => a.ActivityID == val.ActivityID).Points;

                    temp += db.Activities.FirstOrDefault(a => a.ActivityID == val.ActivityID).Points;

                    if (temp > highestDaily) highestDaily = temp;

                    if (temp < lowestDaily) lowestDaily = temp;

                    countDaily++;
                }

                if(countDaily > 0)
                {
                    DateTime StartDate = userA[0].Timestamp;
                    DateTime EndDate = userA[userA.Count-1].Timestamp;

                    int totalDays = (int)(EndDate - StartDate).TotalDays;

                    int avg1 = 0;
                    int avg2 = 0;
                    if (totalDays > 0)
                    {
                        avg1 = totalDaily / totalDays;
                        avg2 = countDaily / totalDays;
                    }
                    else
                    {
                        avg1 = totalDaily;
                        avg2 = countDaily;
                    }
                    

                    l.AvgDailyScore = avg1;
                    l.HighestDaily = highestDaily;
                    l.AvgDailyCount= avg2;
                    l.HighestWeekScore = HighestDaily(weeklyactivities);
                    l.LowestWeekScore = LowestDaily(weeklyactivities);
                    l.MostProductiveCategory = GetMostProductiveCategory(categories);
                    l.WeeklyActivities = weeklyactivities;
                }
                
                

                return Json(l, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        private int getIndex(List<UserActivity> list, UserActivity activity)
        {
            int i = 0;
            foreach(var a in list)
            {
                if (a == activity) return i;
                i++;
            }
            return -1;
        }

        private int HighestDaily(List<WeeklyActivity> t)
        {
            int c = 0;
            foreach(var a in t)
            {
                if (a.Total > c) c = a.Total;
            }

            return c;
        }

        private int LowestDaily(List<WeeklyActivity> t)
        {
            int c = 999999999;

            foreach (var a in t)
            {
                if (a.Total < c) c = a.Total;
            }

            if (c != 999999999)
                return c;
            else
                return 0;
        }

        private string GetMostProductiveCategory(int[] t)
        {
            int c = 0;
            int index = 1;

            for(int i = 0; i < t.Length; i++)
            {
                if(t[i] > c)
                {
                    c = t[i];
                    index = i;
                }
            }

            if (index > 0)
                return db.CategoryMasters.FirstOrDefault(m => m.CategoryID == index).Description;
            else
                return "none";
        }

        [HttpPost]
        public ActionResult Logout()
        {
            Session["UserID"] = null;

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteUser()
        {
            if (Session["UserID"] != null)
            {
                //get the logged in user's id
                object idObject = Session["UserID"];

                int id = Convert.ToInt32(idObject);

                UserMaster u = db.UserMasters.FirstOrDefault(i => i.UserID == id);

                db.UserMasters.Remove(u);

                db.SaveChanges();
                
                Session["UserID"] = null;

            }
                

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}