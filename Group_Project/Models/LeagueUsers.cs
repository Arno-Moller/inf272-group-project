﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Group_Project.Models
{
    public class LeagueUsers
    {
        public string ProfilePic { get; set; }
        public string LeagueIcon { get; set; }
        public string Name { get; set; }
        public int Points { get; set; }
        public int ID { get; set; }
        public int Position { get; set; }
    }
}