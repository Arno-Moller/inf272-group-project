﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Group_Project.Models
{
    public class PerformanceModel
    {
        public int highest { get; set; }
        public int myIndex { get; set; }
        public int DailyPoints { get; set; }
        public string LeagueName { get; set; }
        public List<LeagueUsers> leagueUsers { get; set; }

        public int AvgDailyScore { get; set; }
        public int HighestDaily { get; set; }
        public int AvgDailyCount { get; set; }

        public int HighestWeekScore { get; set; }
        public int LowestWeekScore { get; set; }

        public string MostProductiveCategory { get; set; }

        public List<WeeklyActivity> WeeklyActivities { get; set; }
    }
}