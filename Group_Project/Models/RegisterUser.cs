﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Group_Project.Models
{
    public class RegisterUser
    {
        private string name;
        private string profilePic;
        private string email;
        private string pw;
        private string privateOrPublic;
        private int selected_question;
        private string answer;
        private int country;
        private int gender;
        private int multiplier;
        private int goal;

        public RegisterUser(string name, string email, string pw, string privateOrPublic, int selected_question, string answer, int country, int gender, int goal)
        {
            this.name = name;
            this.profilePic = "./Images/Players/avatar.png";
            this.email = email;
            this.pw = pw;
            this.privateOrPublic = privateOrPublic;
            this.selected_question = selected_question;
            this.answer = answer;
            this.country = country;
            this.gender = gender;
            this.multiplier = 0;
            this.goal = goal;
        }

        public RegisterUser(string profilePic)
        {
            this.profilePic = profilePic;
        }

        public string Name { get => name; set => name = value; }
        public string ProfilePic { get => profilePic; set => name = profilePic; }
        public string Email { get => email; set => email = value; }
        public string Pw { get => pw; set => pw = value; }
        public string PrivateOrPublic { get => privateOrPublic; set => privateOrPublic = value; }
        public int Selected_question { get => selected_question; set => selected_question = value; }
        public string Answer { get => answer; set => answer = value; }
        public int Country { get => country; set => country = value; }
        public int Gender { get => gender; set => gender = value; }
        public int Multiplier { get => multiplier; set => multiplier = value; }
        public int Goal { get => goal; set => goal = value; }
    }
}