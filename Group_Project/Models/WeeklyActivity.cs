﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Group_Project.Models
{
    public class WeeklyActivity
    {
        public int Total { get; set; }
        public DateTime Week { get; set; }
        public string Date { get; set; }

    }
}