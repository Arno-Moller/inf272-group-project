﻿$("#eyeIcon").on('click', () => {
    
    $("#eyeIcon").toggleClass("fa fa-eye");
    $("#eyeIcon").toggleClass("fa fa-eye-slash");

    $("#pwInput").attr("type") == 'password' ? $("#pwInput").attr("type", 'text') : $("#pwInput").attr("type", 'password');
})

$("#loginBtn").on('click', (event) => {

    //empty username
    if ($("#loginUser").val() == "")
    {
        event.preventDefault();

        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Invalid Username',
            content: 'Please Enter A Username And Try Again',
            type: 'red',
            typeAnimated: true,
            buttons:
            {
                tryAgain:
                {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#loginUser").val("");
                        $("#pwInput").val("");
                    }
                }
            }
        });
    }

    //password too short
    else if ($("#pwInput").val().length < 5) {

        event.preventDefault();

        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Invalid Password',
            content: 'The Password Has To Be At Least 5 Characters Long, Please Try Again',
            type: 'red',
            typeAnimated: true,
            buttons:
            {
                tryAgain:
                {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#loginUser").val("");
                        $("#pwInput").val("");
                    }
                }
            }
        });
    }
})