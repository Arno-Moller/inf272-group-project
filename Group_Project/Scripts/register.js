﻿//display chosen pic
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#avatar').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);


    }

}

//upload image when chosen
$("#image").change(function () {
    readURL(this);

    var formdata = new FormData($('form').get(0));
    CallService(formdata);
});

function takeSurvey() {
    $("#surveyMessageContainer").css("display", "none");

    $("#surveyContainer").css("visibility", "visible");
}

$(document).ready(() => {

    //$("#surveyMessageContainer").css("display", "");

    $("#surveyContainer").css("visibility", "none");
})

$("#confirmPW").on('change', () => {

    let password = $("#pw").val();
    let confirmPW = $("#confirmPW").val();

    if (password !== confirmPW) {

        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Passwords don\'t match',
            content: 'The Passwords You Have Provided Do Not Match, Please Try Again',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }
    else if (password.length < 5) {
        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Passwords Is Too Short',
            content: 'The Password Has To Be At Least 5 Characters Long, Please Try Again',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }

})

//if user clicks on take survey
$("#survey").on('click', (event) => {

    event.preventDefault();

    let name = $("#name").val();
    let email = $("#email").val();
    let pw = $("#pw").val();
    let gridRadios = $("input[name='publicOrPrivate']:checked").val();
    let selected_question = $("input[name='selected_question']:checked").val();
    let answer = $("#answer").val();
    let country = $("#country").val();
    let gender = $("#gender").val();
    let goal = $("#goal").val();


    if (name == "" || email == "" || pw == "" || gridRadios == "" || selected_question == "" || answer == "" || country == "" || gender == "" || goal == "") {
        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Empty Fields',
            content: 'You Left Some Fields Empty, Please Enter All Fields',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }
    else if (pw != $("#confirmPW").val())
    {
        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Passwords don\'t match',
            content: 'The Passwords You Have Provided Do Not Match, Please Try Again',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }
    else {
        let user = [
            name,
            email,
            pw,
            gridRadios,
            selected_question,
            answer,
            country,
            gender,
            goal
        ];

        user = JSON.stringify(user);
        console.log(user);


        $.ajax({
            type: "POST",
            url: '/Account/Survey',
            contentType: "application/json; charset=utf-8",
            data: user,
            dataType: "json",
            async: false,
            cache: false,
            success: function (data) {

                if (data == true)
                    window.location.href = "/Account/returnSurvey";

                else {
                    $.confirm({
                        columnClass: 'col-6 text-left',
                        title: 'Invalid Username Or Email',
                        content: 'An Account Already Exists With The Same Username Or Email',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Try again',
                                btnClass: 'btn-red',
                                action: function () {
                                    $("#pw").val("");
                                    $("#confirmPW").val("");
                                }
                            }
                        }
                    });
                }
            },
            error: function (request, status, error) {
                alert(JSON.stringify(error));
            }
        });
    }
})

//if user submits form
$("#submitForm").on('click', (event) => {

    event.preventDefault();

    let name = $("#name").val();
    let email = $("#email").val();
    let pw = $("#pw").val();
    let gridRadios = $("input[name='publicOrPrivate']:checked").val();
    let selected_question = $("input[name='selected_question']:checked").val();
    let answer = $("#answer").val();
    let country = $("#country").val();
    let gender = $("#gender").val();
    let goal = $("#goal").val();


    if (name == "" || email == "" || pw == "" || gridRadios == "" || selected_question == "" || answer == "" || country == "" || gender == "" || goal == "") {
        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Empty Fields',
            content: 'You Left Some Fields Empty, Please Enter All Fields',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }
    else if ($("#pw").val() != $("#confirmPW").val()) {
        
        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Passwords don\'t match',
            content: 'The Passwords You Have Provided Do Not Match, Please Try Again',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }
    else {



        let user = [
            name,
            email,
            pw,
            gridRadios,
            selected_question,
            answer,
            country,
            gender,
            goal
        ];

        user = JSON.stringify(user);
        console.log(user);


        $.ajax({
            type: "POST",
            url: '/Account/SubmitRegister',
            contentType: "application/json; charset=utf-8",
            data: user,
            dataType: "json",
            async: false,
            cache: false,
            success: function (data) {
                
                if (data == true)
                    window.location.href = "/Home";

                else {
                    $.confirm({
                        columnClass: 'col-6 text-left',
                        title: 'Invalid Username Or Email',
                        content: 'An Account Already Exists With The Same Username Or Email',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Try again',
                                btnClass: 'btn-red',
                                action: function () {
                                    $("#pw").val("");
                                    $("#confirmPW").val("");
                                }
                            }
                        }
                    });
                }
            },
            error: function (request, status, error) {
                alert(JSON.stringify(error));
            }
        });
    }
})

//if user submits form
$("#updateBtn").on('click', (event) => {

    event.preventDefault();

    let name = $("#name").val();
    let pw = $("#pw").val();
    let gridRadios = $("input[name='publicOrPrivate']:checked").val();
    let selected_question = $("input[name='selected_question']:checked").val();
    let answer = $("#answer").val();
    let country = $("#country").val();
    let gender = $("#gender").val();
    let goal = $("#goal").val();


    let user = [
        name,
        pw,
        gridRadios,
        selected_question,
        answer,
        country,
        gender,
        goal
    ];

    user = JSON.stringify(user);
    console.log(user);


    if (name == "" || answer == " " || goal == 0) {
        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Invalid Values',
            content: 'We Picked Up Some Invalid Fields, Please Try Again',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }
    else
    if ($("#pw").val() != $("#confirmPW").val()) {

        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Passwords don\'t match',
            content: 'The Passwords You Have Provided Do Not Match, Please Try Again',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }
    else {

        $.ajax({
            type: "POST",
            url: '/Account/UpdateUser',
            contentType: "application/json; charset=utf-8",
            data: user,
            dataType: "json",
            async: false,
            cache: false,
            success: function (data) {

                if (data == true)
                    window.location.href = "/Home";

                else {
                    $.confirm({
                        columnClass: 'col-6 text-left',
                        title: 'Invalid Username',
                        content: 'An Account Already Exists With The Same Username',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Try again',
                                btnClass: 'btn-red',
                                action: function () {
                                    $("#pw").val("");
                                    $("#confirmPW").val("");
                                }
                            }
                        }
                    });
                }
            },
            error: function (request, status, error) {
                alert(JSON.stringify(error));
            }
        });
    }
})

//display info about daily points
$("#info").on('click', () => {
    $.confirm({
        backgroundDismiss: true,
        columnClass: 'col-6 text-left',
        title: 'Weekly Goal',
        content: 'Your Weekly Goal Is The Amount Of Points You Aim To Achieve Each Week. Reach This Goal To Achieve Streaks And Point Boosts',
        type: 'blue',
        typeAnimated: true,
        buttons: {
            tryAgain: {
                text: 'Ok',
                btnClass: 'btn-blue',
                action: function () {
                    $("#pw").val("");
                    $("#confirmPW").val("");
                }
            }
        }
    });
})

//survey page point calculation
$(".surveyItem").on('click', (event) => {

    let val = $(event.currentTarget).val();

    if (event.currentTarget.checked == true) {
        $("#points").val(parseInt($("#points").val()) + parseInt(val));
    }
    else {
        $("#points").val(parseInt($("#points").val()) - parseInt(val));
    }
})

//shows more info about daily goal
$("#eyeIcon").on('click', () => {
    $("#eyeIcon").toggleClass("fa fa-eye");
    $("#eyeIcon").toggleClass("fa fa-eye-slash");

    $("#pw").attr("type") == 'password' ? $("#pw").attr("type", 'text') : $("#pw").attr("type", 'password');
    $("#confirmPW").attr("type") == 'password' ? $("#confirmPW").attr("type", 'text') : $("#confirmPW").attr("type", 'password');
})

//edit page
$("#sequrity_question_table2 tr").on('click', function () {
    var child = $(this).children();

    if (!child[0].children[0].checked) {
        child[0].children[0].checked = true;
    }

})


//sends image to controller
function CallService(file) {
    $.ajax({
        url: '/Account/UploadImage',
        type: 'POST',
        data: file,
        cache: false,
        processData: false,
        contentType: false,
        success: function (color) {
            ;
        },
        error: function () {
            alert('Error occured');
        }
    });
}