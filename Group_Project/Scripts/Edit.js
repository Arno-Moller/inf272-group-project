function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#avatar').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);

        
    }
    
}

$("#image").change(function(){
    readURL(this);

    var formdata = new FormData($('form').get(0));
    CallService(formdata);
});



function CallService(file) {
    $.ajax({
        url: '/Account/UploadImage',
        type: 'POST',
        data: file,
        cache: false,
        processData: false,
        contentType: false,
        success: function (color) {
            ;
        },
        error: function () {
            alert('Error occured');
        }
    });
}



function takeSurvey()
{
    $("#surveyMessageContainer").css("display", "none");

    $("#surveyContainer").css("visibility", "visible");
}

$("#sequrity_question_table2 tr").on('click', function () {
    var child = $(this).children();

    if (!child[0].children[0].checked) {
        child[0].children[0].checked = true;
    }

})

