﻿$("#submitForm").on('click', (event) => {

    if ($("#otpEnter").val() == 0) {

        event.preventDefault();

        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Invalid OTP',
            content: 'Please Enter The OTP Sent To Your Email Address',
            type: 'red',
            typeAnimated: true,
            buttons:
            {
                tryAgain:
                {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }
    else if ($("#newPass").val().length < 5) {

        event.preventDefault();

        $("#newPass").val("");
        $("#confirmNewPass").val("");

        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Invalid Password',
            content: 'Your New Password Has To Be At Least 5 Characters Long, Please Try Again',
            type: 'red',
            typeAnimated: true,
            buttons:
            {
                tryAgain:
                {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }
    else if ($("#newPass").val() != $("#confirmNewPass").val()) {

        event.preventDefault();

        $("#newPass").val("");
        $("#confirmNewPass").val("");

        $.confirm({
            columnClass: 'col-6 text-left',
            title: 'Invalid Passwords',
            content: 'The Passwords Do Not Match, Please Try Again',
            type: 'red',
            typeAnimated: true,
            buttons:
            {
                tryAgain:
                {
                    text: 'Try again',
                    btnClass: 'btn-red',
                    action: function () {
                        $("#pw").val("");
                        $("#confirmPW").val("");
                    }
                }
            }
        });
    }
})

$("#eyeIcon").on('click', () => {
    $("#eyeIcon").toggleClass("fa fa-eye");
    $("#eyeIcon").toggleClass("fa fa-eye-slash");

    $("#newPass").attr("type") == 'password' ? $("#newPass").attr("type", 'text') : $("#newPass").attr("type", 'password');
    $("#confirmNewPass").attr("type") == 'password' ? $("#confirmNewPass").attr("type", 'text') : $("#confirmNewPass").attr("type", 'password');
})