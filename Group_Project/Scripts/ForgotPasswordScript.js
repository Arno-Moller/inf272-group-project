﻿$("#submitBtn").on('click', (event) => {

    let q = $("#selectQuestion").val();
    let a = $("#questionAnswer").val();
    let e = $("#recoverEmail").val();

    if (q == null || a == "" || e == "") {
        $.confirm({
            backgroundDismiss: true,
            columnClass: "col-6 text-left",
            title: 'Empty Fields',
            content: 'Please Fill All Fields And Try Again',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Try again',
                    btnClass: 'btn-red',
                },
            }
        });

        event.preventDefault();
    }
    

})