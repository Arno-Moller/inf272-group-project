$("#electricity_activities input[type='checkbox']").change(function(){
    if(this.checked)
    {
        var parent = $(this).parent();
        var sibling = $(parent).siblings()[1];
        
        $(sibling).css("visibility", "visible");
    }
    else
    {
        var parent = $(this).parent();
        var sibling = $(parent).siblings()[1];
        
        $(sibling).css("visibility", "hidden");
    }
});

$("#recycling_activities input[type='checkbox']").change(function(){
    if(this.checked)
    {
        var parent = $(this).parent();
        var sibling = $(parent).siblings()[1];
        
        $(sibling).css("visibility", "visible");
    }
    else
    {
        var parent = $(this).parent();
        var sibling = $(parent).siblings()[1];
        
        $(sibling).css("visibility", "hidden");
    }
});

$("#travel_activities input[type='checkbox']").change(function(){
    if(this.checked)
    {
        var parent = $(this).parent();
        var sibling = $(parent).siblings()[1];
        
        $(sibling).css("visibility", "visible");
    }
    else
    {
        var parent = $(this).parent();
        var sibling = $(parent).siblings()[1];
        
        $(sibling).css("visibility", "hidden");
    }
});

$("#water_activities input[type='checkbox']").change(function(){
    if(this.checked)
    {
        var parent = $(this).parent();
        var sibling = $(parent).siblings()[1];
        
        $(sibling).css("visibility", "visible");
    }
    else
    {
        var parent = $(this).parent();
        var sibling = $(parent).siblings()[1];
        
        $(sibling).css("visibility", "hidden");
    }
});

$("#food_activities input[type='checkbox']").change(function(){
    if(this.checked)
    {
        var parent = $(this).parent();
        var sibling = $(parent).siblings()[1];
        
        $(sibling).css("visibility", "visible");
    }
    else
    {
        var parent = $(this).parent();
        var sibling = $(parent).siblings()[1];
        
        $(sibling).css("visibility", "hidden");
    }
});

$("#performanceModalBack, #closePerformance").on('click', function () {
    $("#PerformanceModal").css("visibility", "hidden");

    $("#overlay2").css("visibility", "hidden");
});

$("#accountSettingsModalBack").on('click', function () {
    $("#accountSettingsModal").css("visibility", "hidden");

    $("#overlay2").css("visibility", "hidden");
});

$("#ViewActivities").on('click', function () {

    var id = "#activitiesLbl";

    $('html,body').animate({ scrollTop: $(id).offset().top - $(window).height() / 5 }, 'slow');
});

$(".card-header").on('click', function () {

    var arrow = $(this).children()[1];

    if ($(arrow).hasClass("glyphicon glyphicon-menu-up")) {
        $(arrow).removeClass("glyphicon glyphicon-menu-up");
        $(arrow).addClass("glyphicon glyphicon-menu-down");
    }
    else {
        $(arrow).removeClass("glyphicon glyphicon-menu-down");
        $(arrow).addClass("glyphicon glyphicon-menu-up");
    }
    
});

$(".card-body table tr td").on('click', function () {

    //if ($(this).is(":checkbox"))
    console.log($(this));
    var child = $(this).siblings()[0];

    child = $(child).children()[0];

    if ($(child).is(":checkbox")) {
        if ($(child).is(":checked")) {
            $(child).prop("checked", false);
        }
        else
            $(child).prop("checked", true);
    }
});

$("#activityBack").on('click', function () {
    $("#trendReport").css("visibility", "hidden");

    $("#overlay2").css("visibility", "hidden");
});

//print activity report
$("#activityPrint").on('click', function () {

    var div = document.querySelector("#pdfContent");

    var imgData;

    html2canvas($(".pdfContent"), {
        useCORS: true,
        onrendered: function (canvas) {
            imgData = canvas.toDataURL('img/png');

            //make doc
            var doc = new jsPDF('p', 'pt', [($(".pdfContent").width()/1.3), ($(".pdfContent").height()/1.3)]);

            doc.addImage(imgData, 'PNG', 10, 10, ($(".pdfContent").width()/1.3)-20, ($(".pdfContent").height()/1.3)-20);

            doc.save('activity_trend_report.pdf');

        }
    })

    

    
    

    //$("#trendReport").css("visibility", "hidden");

    //$("#overlay2").css("visibility", "hidden");
});

//print performance report
$("#printPerformanceReport").on('click', function () {

    //var div = document.querySelector("#pdfContent");

    var imgData;

    html2canvas($("#performanceContent"), {
        useCORS: true,
        onrendered: function (canvas) {
            imgData = canvas.toDataURL('img/png');

            //make doc
            var doc = new jsPDF('p', 'pt', [($("#performanceContent").width() / 1.4), ($("#performanceContent").height()/1.4)]);

            doc.addImage(imgData, 'PNG', 20, 20, ($("#performanceContent").width() / 1.4) - 40, ($("#performanceContent").height()/1.4) - 40);

            doc.save('my_performance_report.pdf');

        }
    })

    

    
    

    //$("#trendReport").css("visibility", "hidden");

    //$("#overlay2").css("visibility", "hidden");
});

$("#closeActivityReport").on('click', function () {
    $("#trendReport").css("visibility", "hidden");

    $("#overlay2").css("visibility", "hidden");
});

function showActivityReport() {

    $("#trendReport").css("visibility", "visible");

    $("#overlay2").css("visibility", "visible");

    var id = "#trendReportLbl";

    $('html,body').animate({ scrollTop: $(id).offset().top - $(window).height() / 2 }, 'slow');
}

$(function () {
    var PlaceHolderElement = $('#PlaceHolderHere');
    $('button[data-toggle="ajax-modal"]').click(function (event) {
        var url = $(this).data('url');
        $.get(url).done(function (data) {
            PlaceHolderElement.html(data);
            PlaceHolderElement.find('.modal').modal('show');
        })
    })

    PlaceHolderElement.on('click', '[data-save="modal"]', function (event) {
        var form = $(this).parents('.modal').find('form');
        var actionUrl = form.attr('action');
        var sendData = form.serialize();
        $.post(actionUrl, sendData).done(function (data) {
            PlaceHolderElement.find('.modal').modal('hide');
        })
    })
})

$(".activityRow").on('click', (event) => {

    let val = $(event.currentTarget).attr("data-activityPoints");
    let id = $(event.currentTarget).attr("data-activityID");

    let child = $(event.currentTarget).children();
    child = child[0];
    child = $(child).children();
    child = child[0];

    let add = 'add';

    //add points
    if (!$(child).prop('checked')) {

        $("#points").html(parseInt($("#points").html()) - parseInt(val));

        add = 'sub';
    }
    else {
        $("#points").html(parseInt($("#points").html()) + parseInt(val));
    }

    let arr = [add, id]

    arr = JSON.stringify(arr);

    $.ajax({
        type: "POST",
        url: '/Home/AddSubtractDailyPoints',
        contentType: "application/json; charset=utf-8",
        data: arr,
        dataType: "json",
        async: false,
        cache: false,
        success: function (data) {

            popPerformance();

            if (data == "reached") {
                $.confirm({
                    backgroundDismiss: function () {
                        location.reload();
                    },
                    columnClass: 'col-6 text-left',
                    title: 'Daily Goal Reached!',
                    content: 'Congratulations, You Reached Your Daily Goal! Keep Earning Those Points',
                    type: 'green',
                    typeAnimated: true,
                    buttons: {
                        tryAgain: {
                            text: 'Ok',
                            btnClass: 'btn-green',
                            action: function () {
                                location.reload();
                            }
                        }
                    }
                });
            }
            else if (data == "badge") {
                $.confirm({
                    backgroundDismiss: function() {
                        location.reload();
                    },
                    columnClass: 'col-6 text-left',
                    title: 'Badge Earned!',
                    content: 'Congratulations! You Earned A Badge',
                    type: 'green',
                    typeAnimated: true,
                    buttons: {
                        tryAgain: {
                            text: 'Ok',
                            btnClass: 'btn-green',
                            action: function () {
                                location.reload();
                            }
                        }
                    }
                });
            }
            else if (add != "sub") {
                $.confirm({
                    backgroundDismiss: true,
                    columnClass: 'col-6 text-left',
                    title: 'Activity Completed',
                    content: 'Added Additional ' + val + ' points',
                    type: 'green',
                    typeAnimated: true,
                    buttons: {
                        tryAgain: {
                            text: 'Ok',
                            btnClass: 'btn-green',
                            action: function () {

                            }
                        }
                    }
                });
            }
            else {
                $.confirm({
                    backgroundDismiss: true,
                    columnClass: 'col-6 text-left',
                    title: 'Activity Removed',
                    content: 'Removed ' + val + ' points',
                    type: 'orange',
                    typeAnimated: true,
                    buttons: {
                        tryAgain: {
                            text: 'Ok',
                            btnClass: 'btn-orange',
                            action: function () {
                                $("#pw").val("");
                                $("#confirmPW").val("");
                            }
                        }
                    }
                });
            }
        },
        error: function (request, status, error) {
            alert(JSON.stringify(error));
        }
    });

})

$(".follow").on('click', (event) => {

    let id = $(event.currentTarget).attr("data-id");

    let data = [id, $(event.currentTarget).hasClass("btn-success") ? true : false];

    let row = $(event.currentTarget).parent().parent();

    let Continue = true;

    if (data[1] == true) {
        $("#friendsBody").prepend(row);
        $(event.currentTarget).html("Unfollow");

        $(event.currentTarget).toggleClass("btn-success");
        $(event.currentTarget).toggleClass("btn-secondary");

        data = JSON.stringify(data);

        $.ajax({
            type: "POST",
            url: '/Home/Follow',
            contentType: "application/json; charset=utf-8",
            data: data,
            dataType: "json",
            async: false,
            cache: false,
            success: function (data) {

            },
            error: function (request, status, error) {
                alert(JSON.stringify(error));
            }
        });
    }
    else {

        $.confirm({
            backgroundDismiss: function () {
                location.reload();
            },
            columnClass: 'col-6 text-left',
            title: 'Unfriend',
            content: 'Are You Sure You Want To Unfriend This User?',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Yes',
                    btnClass: 'btn-orange',
                    action: function () {
                        $("#communityBody").prepend(row);
                        $(event.currentTarget).html("Follow");

                        $(event.currentTarget).toggleClass("btn-success");
                        $(event.currentTarget).toggleClass("btn-secondary");

                        data = JSON.stringify(data);

                        $.ajax({
                            type: "POST",
                            url: '/Home/Follow',
                            contentType: "application/json; charset=utf-8",
                            data: data,
                            dataType: "json",
                            async: false,
                            cache: false,
                            success: function (data) {

                            },
                            error: function (request, status, error) {
                                alert(JSON.stringify(error));
                            }
                        });
                    }
                },
                close: {
                    text: 'No',
                    btnClass: 'btn',
                    action: function () {
                        Continue = true;
                    }
                }
            }
        });
        
    }

    
})

$("#searchInput").on('click', () => {
    $("#searchInput").val("");

    if (users != []) {
        for (let i = 0; i < users.length; i++) {
            $("#communityBody").prepend(users[i]);
        }
    }
})

$("#searchInput").on('change', () => {

    if ($("#searchInput").val() == "") {

        if (users != []) {
            for (let i = 0; i < users.length; i++) {
                $("#communityBody").prepend(users[i]);
            }
        }
        
        
    };
});

$("#search").on('click', () => {

    let val = $("#searchInput").val();

    users = $("#communityBody").children();

    

    for (let i = 0; i < users.length; i++) {
        let u = $(users[i]).children();
        u = u[1];
        u = $(u).children();

        if (!$(u).html().toLowerCase().includes(val.toLowerCase())) {
            $(users[i]).detach();
        }
        console.log($(u).html());
    }
})

var users = [];

$(document).ready(() => {

    popPerformance();

})

function popPerformance() {

    $.ajax({
        type: "POST",
        url: '/Home/GetLeagueStats',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        cache: false,
        success: function (data) {

            //console.log(data);

            $("#reportGraphTable, #leagueChartToBePrinted").empty();

            let highest = data.highest;
            let index = data.myIndex;
            let users = data.leagueUsers;
            let daily = data.DailyPoints;
            let league = data.LeagueName;
            let avgDailyScore = data.AvgDailyScore;
            let highestDaily = data.HighestDaily;
            let avgDailyCount = data.AvgDailyCount;
            let highestWeekScore = data.HighestWeekScore;
            let lowestWeekScore = data.LowestWeekScore;
            let mostProductiveCategory = data.MostProductiveCategory;

            $("#reportDailyPoints").html(daily);

            $("#leagueName, #LeagueNameForReport").html(league);

            $("#AvgDailyScore").html(avgDailyScore);

            $("#HighestDaily").html(highestDaily);

            $("#AvgDailyCount").html(avgDailyCount);

            $("#HighestWeekScore, #pb").html(highestWeekScore);

            $("#LowestWeekScore").html(lowestWeekScore);

            $("#MostProductiveCategory").html(mostProductiveCategory);

            //08/10/2020 - 12:33
            var currentdate = new Date();
            var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth() + 1) + "/"
                + currentdate.getFullYear() + " - "
                + currentdate.getHours() + ":"
                + currentdate.getMinutes();

            $("#dateTime").html(datetime);





            $("#currentPos").html("#" + index);
            $("#position").html("* Currently Position " + index);

            let colors = ["warning", "success", "info", "danger", "secondary"];

            for (let i = 0; i < users.length; i++) {

                //console.log(users[i].Position + ", " + index);

                if (users[i].Position == index) {
                    $("#weekPoints, #weekPoints2, #weekPoints3").html(users[i].Points);
                    $("#weeklyPointsgraph").css("width", (users[i].Points / highestWeekScore) * 100 + "%");
                }

                $("#reportGraphTable, #leagueChartToBePrinted").append(
                    '<tr class="text - center">' +
                    '<td width = "40px"> <img class="rounded z-depth-2" width="40px" src="' + users[i].ProfilePic + '" data-toggle="tooltip" data-placement="top" title="' + users[i].Name+'"/></td>' +
                    '<td style="vertical-align: middle;">' +
                    '<div class="progress" style="width: 100%;height: 40px;margin: 0;">' +
                    '<div class="progress-bar progress-bar-' + colors[i % 5] + '" style="width:' + (users[i].Points / highest * 100) + '%;"></div>' +
                    '</div>' +
                    '</td>' +
                    '<td style="width: 60px;"><h5 class="text-secondary"><i class="fa fa-star mr-1" style="color: darkorange;"></i>' + users[i].Points + '</h5></td>' +
                    '</tr >');


            }


            let weeks = data.WeeklyActivities;
            
            let x = [];

            for (let i = 0; i < weeks.length; i++) {
                x.push({
                    y: weeks[i].Total,
                    label: weeks[i].Date
                })
            }

            console.log(x);

            //draw bar graph on report
            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "light2", // "light1", "light2", "dark1", "dark2"
                axisY: {
                    title: "SCORE"
                },
                axisX: {
                    title: "WEEK"
                },
                data: [{
                    type: "column",
                    showInLegend: false,
                    legendMarkerColor: "grey",
                    dataPoints: x
                }]
            });
            chart.render();

        },
        error: function (request, status, error) {
            alert(JSON.stringify(error));
        }
    });
}

$("#logout").on('click', () => {

    $.confirm({
        backgroundDismiss: function () {
            location.reload();
        },
        columnClass: 'col-6 text-left',
        title: 'Log Out',
        content: 'Are You Sure You Want To Log Out?',
        type: 'blue',
        autoClose: 'logoutUser|10000',
        typeAnimated: true,
        buttons: {
            cancel: function () {
                
            },
            logoutUser: {
                text: 'Logout',
                btnClass: 'btn-blue',
                keys: ['enter', 'shift'],
                action: function () {

                    $.ajax({
                        type: "POST",
                        url: '/Home/Logout',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: false,
                        cache: false,
                        success: function (data) {
                            window.location.reload();
                        },
                        error: function (request, status, error) {
                            alert(JSON.stringify(error));
                        }
                    });

                }
            }
        }
    });
})

$("#delete").on('click', () => {

    $.confirm({
        backgroundDismiss: function () {
            location.reload();
        },
        columnClass: 'col-6 text-left',
        title: 'Delete',
        content: 'Are You Sure You Want To Delete Your Account?',
        type: 'red',
        typeAnimated: true,
        buttons: {
            cancel: function () {

            },
            logoutUser: {
                text: 'Delete',
                btnClass: 'btn-red',
                keys: ['enter', 'shift'],
                action: function () {

                    $.ajax({
                        type: "POST",
                        url: '/Home/DeleteUser',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: false,
                        cache: false,
                        success: function (data) {
                            window.location.reload();
                        },
                        error: function (request, status, error) {
                            alert(JSON.stringify(error));
                        }
                    });

                }
            }
        }
    });
})